const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

const app = express();
const port = 3000;

app.use(bodyParser.json());
app.use(cookieParser());

app.get('/', (req, res) => {
  console.log('GET /');
  res.sendFile(`${__dirname}/public/index.html`);
})

app.get('/login', (req, res) => {
  console.log('GET /login');
  res.sendFile(`${__dirname}/public/login.html`);
});

app.get('/homepage', (req, res) => {
  console.log('GET /homepage', req.cookies);
  if (req.cookies.new_homepage === 'on') {
    res.sendFile(`${__dirname}/public/new-homepage.html`);
  } else {
    res.sendFile(`${__dirname}/public/homepage.html`);
  }
});

app.post('/api/login', (req, res) => {
  console.log('POST /api/login');
  if (req.body.login === 'new' && req.body.password === 'pw') {
    res.cookie('new_homepage', 'on', { domain: 'localhost' }); // Set-Cookie: new_homepage=on; Domain=localhost; Path=/
  } else {
    res.clearCookie('new_homepage');
  }
  res.json({});
});

app.listen(port, () => console.log(`Listening on port ${port}`));

